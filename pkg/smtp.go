package pkg

import (
	"fmt"
	"net/smtp"
)

type SMTPC struct {
    Client *smtp.Client
}

func NewClient(host string)(error, SMTPC){
    client, err := smtp.Dial(host)
    if err != nil{
        return err, SMTPC{}
    }
    
    return nil, SMTPC{Client: client}  
}

func (s *SMTPC)Verify(user string){
    fmt.Println(s.Client.Verify(user))
     
}

