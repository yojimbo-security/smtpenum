package cmd

import (
	"bufio"
	"fmt"
	"net/smtp"
	"os"
	"strings"
	"sync"

	"github.com/spf13/cobra"
)

var cfgFile string
var host string
var port int
var users string
var numJobs int

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "smtpenumusers",
	Short: "A brief description of your application",
	Long: `A longer description that spans multiple lines and likely contains
examples and usage of using your application. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: func(cmd *cobra.Command, args []string) { 
        //https://gobyexample.com/worker-pools
        // TODO: add worker pool
        //jobs :=make(chan int, numJobs)        
        wg := new(sync.WaitGroup)        
        f, err := os.Open(users)
        if err != nil {
            panic(err)
        }
        defer f.Close()
        host = fmt.Sprintf("%s:%d", host,port)
        scanner:= bufio.NewScanner(f)
        for scanner.Scan(){
            user:= scanner.Text()
            wg.Add(1)
            go func() {
                client, err := smtp.Dial(host)
                if err != nil{
                        panic(err)
                }
                err = client.Verify(user)
                if strings.Contains(err.Error(), "252"){
                    fmt.Printf("%s\n", user)
                }
                defer client.Close()
                defer wg.Done()
            }()
        }

        wg.Wait()
    },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize()

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	// rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
    rootCmd.Flags().StringVarP(&host, "target", "t", "", "Target to connect to")
    rootCmd.Flags().IntVarP(&port, "port", "p", 25, "Port")
    rootCmd.Flags().StringVarP(&users, "users", "U", "", "Users file")
    rootCmd.Flags().IntVarP(&numJobs, "jogs","j",20, "Number of jobs")
}
