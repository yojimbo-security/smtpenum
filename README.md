<h1 align="center">
  <a href="https://github.com/YojimboSecurity/smtpenum">
    <!-- Please provide path to your logo here -->
    <img src="docs/images/logo.svg" alt="Logo" width="100" height="100">
  </a>
</h1>

<div align="center">
  SMTPEnum
  <br />
  <a href="#about"><strong>Explore the screenshots »</strong></a>
  <br />
  <br />
  <a href="https://github.com/YojimboSecurity/smtpenum/issues/new?assignees=&labels=bug&template=01_BUG_REPORT.md&title=bug%3A+">Report a Bug</a>
  ·
  <a href="https://github.com/YojimboSecurity/smtpenum/issues/new?assignees=&labels=enhancement&template=02_FEATURE_REQUEST.md&title=feat%3A+">Request a Feature</a>
  .
  <a href="https://github.com/YojimboSecurity/smtpenum/issues/new?assignees=&labels=question&template=04_SUPPORT_QUESTION.md&title=support%3A+">Ask a Question</a>
</div>

<div align="center">
<br />

[![Project license](https://img.shields.io/github/license/YojimboSecurity/smtpenum.svg?style=flat-square)](LICENSE)

[![Pull Requests welcome](https://img.shields.io/badge/PRs-welcome-ff69b4.svg?style=flat-square)](https://github.com/YojimboSecurity/smtpenum/issues?q=is%3Aissue+is%3Aopen+label%3A%22help+wanted%22)
[![code with love by YojimboSecurity](https://img.shields.io/badge/%3C%2F%3E%20with%20%E2%99%A5%20by-YojimboSecurity-ff1414.svg?style=flat-square)](https://github.com/YojimboSecurity)

</div>

<details open="open">
<summary>Table of Contents</summary>

- [About](#about)
  - [Built With](#built-with)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
- [Usage](#usage)
- [Roadmap](#roadmap)

</details>

---

## About

Enumerate SMTP Users.

I created this because smtp-enumerate-users was not working for me and I wanted
to replave the script below with something more robust and parallel.

```shell
for i in $(cat footprinting-wordlist.txt)
do
    echo "EHLO mail\nVRFY $i\nquit" | nc "$IP" 25
done
```

<details>
<summary>Screenshots</summary>
<br>

> **[?]**
> Please provide your screenshots here.

|                               Home Page                               |                               Login Page                               |
| :-------------------------------------------------------------------: | :--------------------------------------------------------------------: |
| <img src="docs/images/screenshot.png" title="Home Page" width="100%"> | <img src="docs/images/screenshot.png" title="Login Page" width="100%"> |

</details>

### Built With

- Go

## Getting Started

Clone repository and build with `go build`.
### Prerequisites

- Go  1.23

## Usage

```shell
smtpenumusers -t <ip address> -U <file with usernames>
```

## Roadmap

- Make a gorutine pool so we don't DOS the server if the word list is too large
- Add a check for some long randon word, if we still get back that the user is 
  valid. Inform the user of the cli that there are security rules inplace and 
  may not work.
- Add the ability to check for emails
- Clean up an refactor. Make better.

